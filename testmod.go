package testmod

import "fmt"

// Hi returns a friendly greeting
func HiHi(name string) string {
	return fmt.Sprintf("Hi, %s", name)
}
